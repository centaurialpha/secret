import argparse
import os

from rich import print
from rich.prompt import Prompt

from secret import crud
from secret import ui


DEFAULT_DATABASE_PATH = "/tmp/database.db"


def get_cli_parser():
    parser = argparse.ArgumentParser(__doc__)
    parser.add_argument("-v", "--version", action="store_true")
    parser.add_argument("-s", "--show-passwords", action="store_true")

    group = parser.add_mutually_exclusive_group()
    group.add_argument("-a", "--add", action="store_true")
    group.add_argument("-d", "--delete")
    group.add_argument("-l", "--list", action="store_true")
    group.add_argument("-g", "--grep")

    return parser


def check_master_password(func):
    def _inner(*args, **kwargs):
        MAX_ATTEMPTS = 3
        master_password = crud.fetch_master_password()
        for _ in range(MAX_ATTEMPTS):
            user_input = Prompt.ask("[bold blue]Master password", password=True)
            if user_input == master_password:
                func(*args, **kwargs)
                return
            print("[bold red]Nop!")
        print("[bold red]Chau :sad:")

    return _inner


@check_master_password
def add():
    entry = ui.input_entry()
    crud.create(entry)


@check_master_password
def ls(args):
    all_entries = crud.fetch_all()
    ui.show_entries(all_entries, show_passwords=args.show_passwords)


@check_master_password
def grep(args):
    all_entries = crud.fetch(query=args.grep)
    ui.show_entries(all_entries, show_passwords=args.show_passwords)


def run():
    args = get_cli_parser().parse_args()
    if args.version:
        from secret import __version__

        print(f"[b green]{__version__}")
        return

    if not os.path.exists(DEFAULT_DATABASE_PATH):
        crud.init(DEFAULT_DATABASE_PATH)
        user_input = Prompt.ask("[bold blue]New master password", password=True)
        crud.set_master_password(user_input)

    if args.list:
        ls(args)
    elif args.add:
        add()
    elif args.grep:
        grep(args)
