from rich import print as rprint
from rich.prompt import Prompt
from rich.table import Table

from secret.crud import Entry


def input_entry():
    title = Prompt.ask("[bold blue]Title")
    user = Prompt.ask("[bold blue]User")
    password = Prompt.ask("[bold blue]Password", password=True)
    comment = Prompt.ask("[bold blue]Comment")

    return Entry(title=title, user=user, password=password, comment=comment)


def show_entries(data, show_passwords=False):
    entries = []
    for item in data:
        entries.append(
            Entry(title=item[1], user=item[2], password=item[3], comment=item[4])
        )

    table = Table(title="Entries")

    table.add_column("#")
    table.add_column("Title")
    table.add_column("User")
    table.add_column("Password")
    table.add_column("Comment")

    for e, entry in enumerate(entries):
        password = entry.password if show_passwords else "**********"
        table.add_row(str(e + 1), entry.title, entry.user, password, entry.comment)

    rprint(table)
