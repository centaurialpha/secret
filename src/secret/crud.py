"""
CRUD operations
"""
import sqlite3
from dataclasses import dataclass


DEFAULT_DATABASE_PATH = "/tmp/database.db"


@dataclass
class Entry:
    title: str
    user: str
    password: str
    comment: str


def _get_connection(path: str = None):
    if path is None:
        path = DEFAULT_DATABASE_PATH
    conn = sqlite3.connect(path)
    return conn


def _create_tables(path):
    conn = _get_connection(path)
    with conn:
        conn.execute(
            "CREATE TABLE entries "
            "(id INTEGER PRIMARY KEY, title TEXT, user TEXT, "
            "password TEXT, comment TEXT)"
        )
        conn.execute("CREATE TABLE master " "(masterpass TEXT)")


def set_master_password(password: str):
    conn = _get_connection()
    with conn:
        conn.execute("INSERT INTO master (masterpass) VALUES (?)", (password,))


def fetch_master_password():
    conn = _get_connection()
    with conn:
        data = conn.execute("SELECT masterpass FROM master")
        return data.fetchone()[0]


def init(path: str):
    _create_tables(path)


def create(entry: Entry):
    conn = _get_connection()
    with conn:
        conn.execute(
            "INSERT INTO entries (title, user, password, comment) "
            "VALUES (?, ?, ?, ?)",
            (entry.title, entry.user, entry.password, entry.comment),
        )


def fetch_all():
    conn = _get_connection()
    with conn:
        data = conn.execute("SELECT * FROM entries")
        return data.fetchall()


def read(identifier: int):
    conn = _get_connection()
    with conn:
        data = conn.execute("SELECT * FROM entries WHERE id=?", (identifier,))
        return data.fetchall()


def fetch(query: str):
    a, b = query.split("=")
    conn = _get_connection()
    with conn:
        data = conn.execute(f"SELECT * FROM entries WHERE {a}=?", (b,))
        return data.fetchall()


def update(identifier: int):
    pass


def delete(identifier: int):
    pass
